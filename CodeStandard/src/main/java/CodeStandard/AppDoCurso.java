package CodeStandard;

public class AppDoCurso {
    public static void main( String[] args ){

        Pessoa jaum = new Pessoa();
        jaum.setIdade(50);
        jaum.setNome("João");

        System.out.println( "Oi, eu sou o " + jaum.getNome() + " e tenho " + jaum.getIdade() + " anos." );
    }
}
