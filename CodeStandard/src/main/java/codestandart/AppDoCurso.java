package codestandart;

public class AppDoCurso {
    public static void main( String[] args ){

        Pessoa joao = new Pessoa();
        joao.setIdadePessoa(50);
        joao.setNomePessoa("João");

        System.out.println( "Oi, eu sou o" + joao.getNomePessoa() + ", e tenho " + joao.getIdadePessoa() + " anos." );
    }
}
